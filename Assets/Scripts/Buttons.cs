using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour
{
    [SerializeField] private Performer performer;

    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private int emmitAmmount;

    public void Forward()
    {
        performer.SetStrategy(new MoveForward(moveSpeed));
    }

    public void Rotate()
    {
        performer.SetStrategy(new Rotate(rotateSpeed));
    }

    public void Emmit()
    {
        performer.SetStrategy(new Emmit(emmitAmmount));
    }
}
