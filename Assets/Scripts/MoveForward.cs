using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : IStrategy
{
    [SerializeField] private float speed;

    public MoveForward(float _speed)
    {
        speed = _speed;
    }

    public void Perform(Transform transform)
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }
}
