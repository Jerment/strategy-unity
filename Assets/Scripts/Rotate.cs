using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : IStrategy
{
    [SerializeField] private float speed;

    public Rotate(float _speed)
    {
        speed = _speed;
    }

    public void Perform(Transform transform)
    {
        transform.Rotate(Vector3.forward, Time.deltaTime * speed);
    }
}
