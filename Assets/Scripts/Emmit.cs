using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emmit : IStrategy
{
    [SerializeField] private int amount;

    public Emmit(int _amount)
    {
        amount = _amount;
    }

    public void Perform(Transform transform)
    {
        ParticleSystem particleSystem = GameObject.FindObjectOfType<ParticleSystem>().GetComponent<ParticleSystem>();
        ParticleSystem.MainModule main = particleSystem.main;
        main.maxParticles = amount;
        particleSystem.Emit(amount);
    }
}
