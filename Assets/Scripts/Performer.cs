using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Performer : MonoBehaviour
{
    [SerializeField] private IStrategy strategy;
    [SerializeField] private Transform _transform;

    public void SetStrategy(IStrategy _strategy)
    {
        strategy = _strategy;
    }

    private void Update()
    {
        strategy.Perform(_transform);
    }
}
